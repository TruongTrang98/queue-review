const express = require('express');
const axios = require('axios');
const Redis = require('redis');
const fs = require('fs');
const Async = require('async');

const app = express();
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

app.post('/job', (req, res) => {
  // Prepare some stuff
  console.log(req.body);
  try {
    // Do something heavy by push into queue
    const _async = Async.cargo((task, callback) => {
      try {
        (async () => {
          try {
            console.log(`Call API ${task.name}`);
            if (!task) {
              return false;
            }
            const result = await axios.get(`${task.url}`);
            fs.writeFileSync(`./data/${task.name}.json`, JSON.stringify(result.data));
          } catch (error) {
            console.log(error);
            throw new Error(error);
          }
        })()
      } catch (error) {
        return error;
      } finally {
        callback();
      }
    })

    _async.push(req.body)
    return res.status(200).json({
      message: "Success"
    })
  } catch (error) {
    console.log(error)
    res.status(200).json({
      message: "Fail"
    })
  }
})

app.listen(3000, () => {
  console.log(`Server is running at port 3000`)
})


