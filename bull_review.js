const express = require('express');
const axios = require('axios');
const Queue = require('bull');
const Redis = require('redis');
const fs = require('fs');

const client = new Redis.createClient({
  host: '127.0.0.1',
  port: '6379'
})

client.on('error', err => {
  throw new Error(err);
})

client.on('connect', () => {
  console.log('Redis Connected');
})

const myJobQueue = new Queue('myJob', 'redis://127.0.0.1:6379');
const app = express();
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

myJobQueue.process((job, done) => {
  console.log(job.data);
  console.log(`Calling API ${job.data.name}`);
  axios.get(`${job.data.url}`)
    .then(res => {
      console.log(res.data);
      fs.writeFileSync(`./data/${job.data.name}.json`, JSON.stringify(res.data))
    })
    .catch(err => {
      throw new Error(err);
    })
  done();
})

app.post('/job', (req, res) => {
  // Prepare some stuff
  console.log(req.body);
  try {
    // Do something heavy by push into queue
    myJobQueue.add(
      { ...req.body }
    )
    return res.status(200).json({
      message: "Success"
    })
  } catch (error) {
    res.status(200).json({
      message: "Fail"
    })
  }
})

app.listen(3000, () => {
  console.log(`Server is running at port 3000`)
})


